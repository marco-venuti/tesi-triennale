\documentclass[mathserif]{beamer}
\usepackage[italian]{babel}
\usepackage[utf8]{inputenc}
\usepackage{commath}
\usepackage{mathtools}
\usepackage{braket}
\usepackage{biblatex}
\addbibresource{../tesi/biblio.bib}

\renewcommand{\familydefault}{\sfdefault}
\usepackage{siunitx}
\sisetup{
  detect-mode=true,detect-family=false,
  detect-display-math=false,detect-shape=false
}
\AtBeginDocument{\sisetup{math-rm=\mathrm, text-rm=\sffamily}}

\DeclareMathOperator{\tr}{tr}
\newcommand{\mean}[1]{\langle #1 \rangle}
\newcommand{\Mean}[1]{\left< #1 \right>}

\usetheme{Madrid}
\setbeamertemplate{navigation symbols}{}

\title{Path-integral Monte Carlo}
\author[Marco Venuti]{Candidato: Marco Venuti\\ Relatore: Claudio Bonati}
\date{17 settembre 2020}


\begin{document}

\begin{frame}
    \maketitle
    \begin{center}
        \includegraphics[scale=0.2]{marchio_unipi}
    \end{center}
\end{frame}

\begin{frame}{Path-integral formalism} %1
    \begin{itemize}

    \item Propagatore: funzione di Green del problema
        \[
            i\hbar \dpd{\psi}{t} = \hat{H} \psi(x,t) \quad\Rightarrow\quad
            \psi(x, t) = \int \dif{y} \braket{x | e^{-\frac{i}{\hbar}\hat{H}t} | y} \psi(y, 0)
        \]
        \pause
    \item Si riscrive come integrale funzionale
        \[
            \braket{x_{f} | e^{-\frac{i}{\hbar}\hat{H} t} | x_{i}} = \int\limits_{\substack{0\to t\\ x_{i}\to x_{f}}} [\Dif{x}]\ e^{\frac{i}{\hbar} S[x, \dot{x}]}
        \]
        \pause

    \item Legame con la statistica: \( t \to -i\hbar\beta = -i \frac{\hbar}{k T} \)
        \[
            \mathcal{Z} = \tr\sbr{e^{-\beta \hat{H}}} =
            \int \dif{x} \braket{x | e^{-\beta\hat{H}} | x} =
            \int\limits_{\substack{0\to \hbar\beta\\ x(0) = x(\hbar\beta)}}[\Dif{x}] e^{-\frac{1}{\hbar}S_{E}[x, \dot{x}]}
        \]
        \[ S_{E}[x, \dot{x}] = \int_{0}^{\hbar\beta} \dif{t}\, \mathcal{L}_{E}(x(t), \dot{x}(t)) = \int_{0}^{\hbar\beta} \dif{t} \sbr{\frac{1}{2}m (\dot{x}(t))^{2} + V(x(t))} \]
    \end{itemize}

\end{frame}

\begin{frame} %2

    \begin{itemize}

    \item Medie di osservabili (se dipendono solo dalla posizione)
        \[
            \mean{f(\hat{x}(\tau))} = \frac{1}{\mathcal{Z}} \tr\sbr{e^{-\beta \hat{H}}f(\hat{x}(\tau))}
            = \frac{1}{\mathcal{Z}} \hspace{-0.4cm} \int\limits_{\substack{0\to \hbar\beta\\ x(0) = x(\hbar\beta)}} [\Dif{x}]e^{-\frac{1}{\hbar}S_{E}[x, \dot{x}]} f(x(\tau))
        \]
        \pause

    \item Campionamento delle traiettorie secondo la distribuzione
        \[ \frac{1}{\mathcal{Z}} [\Dif{x}]e^{-\frac{1}{\hbar}S_{E}[x, \dot{x}]} \]
        \pause

    \item Nella pratica il path-integral si discretizza
        \[
            \braket{x_{f} | e^{-\beta\hat{H}} | x_{i}} =
            \int \dif{x_{2}}\cdots \dif{x_{N}} \del{\frac{mN}{2\pi\hbar t}}^{N/2}
            \exp\sbr{-\frac{1}{\hbar} S_{E}(x_{1}, \ldots, x_{N})}
        \]
        \[
            S_{E}(x_{1}, \ldots, x_{N}) = \sum_{j=1}^{N}\del{\frac{t}{N}}\del{\frac{m}{2} \frac{(x_{j+1}-x_{j})^{2}}{(t/N)^{2}} + V(x_{j}) }
        \]
    \item Stima del valore medio e analisi degli errori

    \end{itemize}

\end{frame}

\begin{frame}{Metropolis Monte Carlo} %3

    \begin{itemize}

    \item Problema: estrarre traiettorie secondo la distribuzione assegnata \( p_{j} \).
        \pause

    \item Catena di Markov: grafo con probabilità di transizione \( P_{ij} \).

    \item \( P_{ij} \) irriducibile \( \Rightarrow \) le frequenze di visita tendono a una distribuzione di equilibrio (invariante).
        \pause

    \item Uso una catena di Markov tentativa \( P_{ij} \). Accetto la mossa \( i\to j \) con probabilità
        \[ T_{ij} = P_{ij}\min\left\{1, \frac{p_{j}}{p_{i}}\right\} \]

    \item Se \( P_{ij} = P_{ji} \) è irriducibile, \( T_{ij} \) è irriducibile e in bilancio dettagliato con \( p_{j} \). La distribuzione di equilibrio è \( p_{j} \).
        \pause

    \item Parto da una data configurazione (e.g. \( x_{j} = 0\ \ \forall j \)), estraggo traiettorie all'equilibrio.

    \item Le misure sono correlate nel tempo Monte Carlo.

    \end{itemize}

\end{frame}

\begin{frame}{Osservabili} %4
    Nel limite di bassa temperatura \( \beta\to + \infty \), \( \frac{1}{\mathcal{Z}}\tr\sbr{e^{-\beta \hat{H}} \hat{\Theta}} \to \braket{0 | \hat{\Theta} | 0} \)

    \begin{itemize}

    \item Energia del fondamentale: teorema del viriale
        \[ 2\braket{0 | \hat{T} | 0} = \braket{0 | \hat{x}\, V'(\hat{x}) | 0} \]
        Per l'oscillatore armonico
        \[ E_{0} = 2\Braket{0 | \textstyle\frac{1}{2}m\omega^{2} \hat{x}^{2} | 0} \]
        \pause

    \item Primi gap (\( V(x) = V(-x) \))
        \[
            \braket{0 | e^{\hat{H} \tau / \hbar} \hat{x} e^{-\hat{H} \tau / \hbar} \hat{x} | 0}
            = \sum_{n} e^{-\tau \Delta_{n0}/\hbar} \abs{\braket{0 | \hat{x} | n}}^{2}
            \simeq e^{-\tau \Delta_{10}/\hbar} \abs{\braket{0 | \hat{x} | 1}}^{2}
        \]
        \pause
        \[
            \braket{0 | e^{\hat{H} \tau / \hbar} \hat{x}^{2} e^{-\hat{H} \tau / \hbar} \hat{x}^{2} | 0}
            \simeq \abs{\braket{0 | \hat{x}^{2} | 0}}^{2} + e^{-\tau \Delta_{20}/\hbar} \abs{\braket{0 | \hat{x}^{2} | 2}}^{2}
        \]
        \pause

    \item Forma del fondamentale
        \[
            \frac{1}{\mathcal Z}\tr[e^{-\beta \hat{H}} \delta(y-x)] \simeq \mean{0 | \delta(y-x) | 0} = \abs{\psi_{0}(y)}^{2}
        \]

    \end{itemize}

\end{frame}

\begin{frame}{Risultati} %5
    \setlength{\abovedisplayskip}{2pt}
    \setlength{\belowdisplayskip}{2pt}
    \def\scale{0.8\textwidth}
    \begin{columns}[T]
        \begin{column}{0.5\textwidth}
            \vspace{0.2cm}
            \only<1->{
              In unità \( \hbar = m = \omega = 1 \)
            }
            \begin{itemize}
            \item<2-> Per \( t = 50 \), \( N = 100, \ldots, 500 \)
                \[ E_{0} = \num{0.5000 +- 0.0005} \]
            \item<3-> Per \( t = 30 \), \( N = 300 \)
                \begin{align*}
                  E_{1} - E_{0} &= \num{0.9992 +- 0.0009} \\
                  E_{2} - E_{0} &= \num{1.9985 +- 0.0029}
                \end{align*}
            \end{itemize}
        \end{column}
        \begin{column}{0.5\textwidth}<2->
            \centering
            \includegraphics[width=\scale]{energy-t.png}
        \end{column}
    \end{columns}

    \begin{columns}[b]
        \begin{column}{0.5\textwidth}<4->
            \centering
            \includegraphics[width=\scale]{../sim/path-integral/wavefunction.png}
        \end{column}
        \begin{column}{0.5\textwidth}<3->
            \centering
            \includegraphics[width=\scale]{corr-eff-mass.png}
        \end{column}
    \end{columns}
    \vspace{-0.2cm}
    {\tiny Il codice è disponibile su \url{https://gitlab.com/marco-venuti/tesi-triennale}.}
\end{frame}

\begin{frame}[b]
    \vspace{-1cm}\begin{center}
        \huge
        Grazie per l'attenzione!
    \end{center}
    \vfill
    \renewcommand*{\bibfont}{\footnotesize}
    \nocite{*}\printbibliography%
\end{frame}

\begin{frame}<handout:0>[noframenumbering]
    \begin{align*}
      E_{1} - E_{0} &= -\frac{1}{\Delta\tau} \log\sbr{\frac{\Mean{x(\tau+\Delta\tau)x(0)}_{P} - \Mean{x(0)}_{P}^{2}}{\Mean{x(\tau)x(0)}_{P} - \Mean{x(0)}_{P}^{2}}} \\
      E_{2} - E_{0} &= -\frac{1}{\Delta\tau}
                      \log\sbr{ \frac{\mean{x^{2}(\tau + \Delta\tau) x^{2}(0)}_{P} - \mean{x^{2}(0)}^{2}_{P}}{\mean{x^{2}(\tau) x^{2}(0)}_{P} - \mean{x^{2}(0)}^{2}_{P}} }
    \end{align*}

    Per l'oscillatore armonico
    \begin{align*}
      E_{1} - E_{0} &= -\frac{1}{\Delta\tau} \log\sbr{\frac{\Mean{x(\Delta\tau)x(0)}_{P}}{\Mean{x^{2}(0)}_{P}}} \\
      E_{2} - E_{0} &= -\frac{1}{\Delta \tau} \log\sbr{\frac{\mean{x^{2}(\Delta\tau) x^{2}(0)}_{P} - \mean{x^{2}(0)}^{2}_{P}}{\mean{x^{4}(0)}_{P} - \mean{x^{2}(0)}^{2}_{P}} }
    \end{align*}
\end{frame}

\end{document}
