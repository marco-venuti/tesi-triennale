#!/usr/bin/env python3

import matplotlib.pyplot as plt
import numpy as np
import random

P = np.array([
    [0.1, 0.8, 0.1],
    [0.4, 0.5, 0.1],
    [0.3, 0.3, 0.4]
])


def step(vec):
    return vec @ P


def genSequence(vec, steps):
    seq = []
    vec = np.array(vec)
    for i in range(steps):
        seq.append(vec)
        vec = step(vec)
    return np.array(seq)


fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.set_xlim(0, 1)
ax.set_ylim(0, 1)
ax.set_zlim(0, 1)
ax.set_xlabel('x')
ax.set_ylabel('y')
ax.set_zlabel('z')

random.seed()
for i in range(20):
    a = random.random()
    b = random.random()
    m = min([a, b])
    M = max([a, b])
    seq = genSequence([m, M-m, 1-M], 200)
    ax.plot(seq[:, 0], seq[:, 1], seq[:, 2], marker='.', linestyle=':')

plt.show()
# plt.savefig('markov.png')
