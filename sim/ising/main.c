/********************************
 * Modello di Ising per sport.
 ********************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>
#define latticeSize 100

extern inline int pmod(int i, int n) {
    return (i % n + n) % n;
}

void printLattice(char** lattice)
{
    for (int i=0; i<latticeSize; i++)
    {
        for (int j=0; j<latticeSize; j++)
            printf("%s ", lattice[i][j] == 1 ? " 1" : "-1");
        printf("\n");
    }
}

void fprintLattice(FILE* file, char **lattice)
{
    for (int i=0; i<latticeSize; i++)
        for (int j=0; j<latticeSize; j++)
            fprintf(file, "%d ", lattice[i][j]);
    fprintf(file, "\n");
}

void freeLattice(char **lattice)
{
    for (int i=0; i<latticeSize; i++)
        free(lattice[i]);
    free(lattice);
}

void boltzExtract(char** lattice, double beta, double J, unsigned steps /* ,FILE* file */)
{
    // Monte Carlo
    double deltaE;
    unsigned a, b;
    for (int step=0; step<steps; step++)
    {
        /* if(!(step % 3000000)) */
        /*     fprintLattice(file, lattice); */

        // TODO: si potrebbe fare flip&swap
        // choose random spin
        a = rand() % latticeSize;
        b = rand() % latticeSize;

        // Metropolis move
        deltaE = 2*J*lattice[a][b]*(
            lattice[pmod(a,latticeSize)][pmod(b+1,latticeSize)] +
            lattice[pmod(a,latticeSize)][pmod(b-1,latticeSize)] +
            lattice[pmod(a+1,latticeSize)][pmod(b,latticeSize)] +
            lattice[pmod(a-1,latticeSize)][pmod(b,latticeSize)]
            );

        if (deltaE <= 0) // Surely flip (downhill move)
            lattice[a][b] *= -1;
        else // Maybe flip (uphill move)
            if ((double)rand()/RAND_MAX < exp(-beta*deltaE))
                lattice[a][b] *= -1;
    }
}

double hamiltonian(char** lattice, double J)
{
    double energy = 0;
    for (int i=0; i<latticeSize; i++)
        for (int j=0; j<latticeSize; j++)
            energy += lattice[i][j] * (lattice[(i+1) % latticeSize][j] + lattice[i][(j+1) % latticeSize]);
    return -energy*J;
}

double relativeMangetization(char** lattice)
{
    double mag = 0;
    for (int i=0; i<latticeSize; i++)
        for (int j=0; j<latticeSize; j++)
            mag += lattice[i][j];
    return mag/(latticeSize*latticeSize);
}

/* int main(int argc, char *argv[]) */
/* { */
/*     double J = 1; */
/*     unsigned metropolisSteps = 1000; */
/*     double beta = 1; */

/*     FILE* outFile = fopen("boltz.txt", "w"); */
/*     for (int i=0; i < 1e5; i++) */
/*         fprintLatticeInline(outFile, hamiltonian(boltzExtract(beta, J, metropolisSteps), J)); */
/*         // fprintf(outFile, "%f\n", hamiltonian(boltzExtract(beta, J, metropolisSteps), J)); */
/*     fclose(outFile); */

/*     return 0; */
/* } */

/* // Fixed temperature */
/* int main(int argc, char *argv[]) */
/* { */
/*     double J = 1; */
/*     double beta = 4; */
/*     unsigned N = 6000*(latticeSize*latticeSize); */

/*     // Initialize lattice */
/*     char** lattice = (char**) malloc(latticeSize * sizeof(char **)); */
/*     for(int i = 0; i < latticeSize; i++) lattice[i] = (char *) malloc(latticeSize * sizeof(char)); */

/*     srand(time(0)); */
/*     for (int i=0; i<latticeSize; i++) */
/*         for (int j=0; j<latticeSize; j++) */
/*             lattice[i][j] = rand() % 2 ? 1 : -1; */

/*     FILE* outFile = fopen("fixedt.txt", "w"); */

/*     boltzExtract(lattice, beta, J, N, outFile); */
/*     freeLattice(lattice); */
/*     fclose(outFile); */
/*     return 0; */
/* } */


int main(int argc, char *argv[])
{
    double J = 1;
    unsigned Nmean = 100;

    // Initialize lattice
    char** lattice = (char**) malloc(latticeSize * sizeof(char **));
    for(int i = 0; i < latticeSize; i++) lattice[i] = (char *) malloc(latticeSize * sizeof(char));

    srand(time(0));
    for (int i=0; i<latticeSize; i++)
        for (int j=0; j<latticeSize; j++)
            /* lattice[i][j] = 1; */
            lattice[i][j] = rand() % 2 == 0 ? 1 : -1; // high temp limit

    double meanEnergy, energy, meanEnergy2, meanMag;

    FILE* outFile = fopen("out.txt", "w");
    fprintf(outFile, "# beta U mag specHeat\n");

    /* boltzExtract(lattice, 10, J, 6e3*latticeSize*latticeSize); */
    for (double beta=0; beta<1; beta+=0.05)
    {
        meanEnergy = 0;
        energy = 0;
        meanEnergy2 = 0;
        meanMag = 0;
        printf("Computing for beta = %f\n", beta);
        for (int i=0; i<Nmean; i++)
        {
            boltzExtract(lattice, beta, J, 50*latticeSize*latticeSize);
            energy = hamiltonian(lattice, J);
            meanEnergy += energy;
            meanEnergy2 += energy*energy;
            meanMag += relativeMangetization(lattice);
        }

        meanEnergy /= Nmean*J*latticeSize*latticeSize;
        meanEnergy2 /= Nmean*pow(J*latticeSize*latticeSize, 2);
        meanMag /= Nmean;

        fprintf(outFile, "%f %f %f %f\n",
                beta, meanEnergy, meanMag,
                beta*beta*(meanEnergy2-meanEnergy*meanEnergy)
            );
    }
    freeLattice(lattice);
    fclose(outFile);
    return 0;
}
