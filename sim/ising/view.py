#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt

beta, U, mag, specHeat = np.loadtxt('build/out.txt', unpack=True)

plt.plot(beta, U)
plt.plot(beta, mag)
plt.plot(beta, specHeat)
plt.legend(['U', 'mag', 'specHeat'])
# plt.ylim(-1.1, 1.1)
# plt.xscale('log')
plt.xlabel('$\\beta$')
# plt.ylabel('$U/(JN)$')
# plt.ylabel('$M$')
# plt.savefig('ising.png')
plt.show()
