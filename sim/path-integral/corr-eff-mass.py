#!/usr/bin/env python3

import numpy as np
from sys import argv
from uncertainties import (
    correlated_values, ufloat,
    unumpy as unp
)
import matplotlib.pyplot as plt
from math import nan
nv = unp.nominal_values
sd = unp.std_devs

# simulation parameters
path1 = argv[1]
path2 = argv[2]
t = float(argv[3])
N = int(argv[4])
tau1 = 0
tau2 = 0

print(f't = {t}')
print(f'N = {N}\n')

# load data
data1 = np.loadtxt(path1).transpose()
data2 = np.loadtxt(path2).transpose()


# E_1 - E_0
covm1 = np.cov(data1)/len(data1[0])  # covariance matrix of the means
Ntau1 = len(data1)

correlator1 = correlated_values([d.mean() for d in data1], covm1)
gap10 = []

for i in range(tau1+1, Ntau1):
    try:
        gap10.append(
            -unp.log(correlator1[i]/correlator1[tau1])*N/t/i
        )
    except ValueError:
        gap10.append(ufloat(nan, nan))


# E_2 - E_0
covm2 = np.cov(data2)/len(data2[0])  # covariance matrix of the means
Ntau2 = len(data2)

# Faccio una schifezza per incollare la matrice di covarianza del
# correlatore di x² con il vettore delle correlazioni tra x(0) e x²(t)
corr21 = np.array([
    ((d-d.mean())*(data1[0]-data1[0].mean())).sum()
    for d in data2
])/(len(data2[0])-1)/len(data2[0])

covm2 = np.vstack((covm2, corr21))
corr21 = np.concatenate((corr21, [covm1[0][0]]))
corr21.shape = (len(corr21), 1)
covm2 = np.hstack((covm2, corr21))

tmp = correlated_values([d.mean() for d in data2] + [correlator1[0].nominal_value], covm2)
correlator2 = tmp[:-1]
z = tmp[-1]

gap20 = []

for i in range(tau2+1, Ntau2):
    try:
        gap20.append(
            -unp.log(
                (correlator2[i]-z**2)/(correlator2[tau2]-z**2)
            )*N/t/i
        )
    except ValueError:
        gap20.append(ufloat(nan, nan))

# Risultati e grafici
print('Gap E_1 - E_0')
for i in gap10:
    print(f'{i:.5f} {(i.nominal_value-1)/i.std_dev:.2f} {100*i.std_dev/i.nominal_value:.2f}')

print('\nGap E_2 - E_0')
for i in gap20:
    print(f'{i:.5f} {(i.nominal_value-2)/i.std_dev:.2f} {100*i.std_dev/i.nominal_value:.2f}')

# Faccio le medie pesate
# Uncertainties tiene conto delle correlazioni in modo automagico

delta10 = sum(gap10/(sd(gap10)**2)) / sum(1/(sd(gap10)**2))
delta20 = sum(gap20/(sd(gap20)**2)) / sum(1/(sd(gap20)**2))
print(f'\nE_1 - E_0 = {delta10:.5f} ({(delta10.nominal_value-1)/delta10.std_dev:.2f})')
print(f'E_2 - E_0 = {delta20:.5f} ({(delta20.nominal_value-2)/delta20.std_dev:.2f})')

# Faccio i grafici
plt.style.use('style.yml')
fig, ax = plt.subplots(2, 1, sharex=True)
plt.subplots_adjust(hspace=0.15)

ax[0].errorbar(
    range(1, Ntau1), nv(gap10), yerr=sd(gap10),
    marker='.', markersize=3, linestyle='', color='red', capsize=1, capthick=0.5
)
ax[0].plot([1, Ntau1], [1, 1], color='blue')
ax[0].set_ylabel('$E_1 - E_0$')
ax[0].set_ylim(0.99, 1.01)
ax[1].errorbar(
    range(1, Ntau2), nv(gap20), yerr=sd(gap20),
    marker='.', markersize=3, linestyle='', color='red', capsize=1, capthick=0.5
)
ax[1].plot([1, Ntau2], [2, 2], color='blue')
ax[1].set_ylim(1.99, 2.01)
ax[1].set_xlabel(r'$\Delta\tau$')
ax[1].set_ylabel('$E_2 - E_0$')
plt.savefig('corr-eff-mass.png')
# plt.show()
