#!/usr/bin/env python3

import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from uncertainties import correlated_values, unumpy as unp
import numpy as np
from sys import argv

data1 = np.loadtxt(argv[1]).transpose()
data2 = np.loadtxt(argv[2]).transpose()

t = float(argv[3])
N = int(argv[4])

avg1 = np.array([d.mean() for d in data1])
avg2 = np.array([d.mean() for d in data2])

std1 = [d.std(ddof=1)/np.sqrt(len(d)) for d in data1]
std2 = [d.std(ddof=1)/np.sqrt(len(d)) for d in data2]


def fit1(x, gap, A):
    return A*np.exp(-x*gap)


def fit2(x, gap, A, B):
    return B + A*np.exp(-x*gap)


X_data = np.array(range(len(data1)))

# Gap E_1 - E_0
opt1, cov1 = curve_fit(
    fit1, X_data, avg1, sigma=std1, absolute_sigma=True
)

opt1 = correlated_values(opt1, cov1)
gap10 = opt1[0]*N/t

# Gap E_2 - E_0
opt2, cov2 = curve_fit(
    fit2, X_data, avg2, sigma=std2, absolute_sigma=True
)

opt2 = correlated_values(opt2, cov2)
gap20 = opt2[0]*N/t

print(f'E_1 - E_0 = {gap10}')
print(f'E_2 - E_0 = {gap20}')
print(f'|〈0|x|1〉|² = {opt1[1]}')
print(f'|〈0|x²|0〉|² = {opt2[2]}')
print(f'|〈0|x²|2〉|² = {opt2[1]}')

X = np.linspace(0, X_data[-1], 1000)

plt.style.use('style.yml')
plt.errorbar(X_data, avg1, yerr=std1, marker='x', linestyle='')
plt.errorbar(X_data, avg2, yerr=std2, marker='x', linestyle='')
plt.plot(X, fit1(X, *unp.nominal_values(opt1)))
plt.plot(X, fit2(X, *unp.nominal_values(opt2)))
plt.legend([
    r'$\langle x(\tau)x(0)\rangle$',
    r'$\langle x^2(\tau)x^2(0)\rangle$',
])
plt.xlabel(r'$\tau$')
plt.savefig('corr-harm-osc.png')
# plt.show()
