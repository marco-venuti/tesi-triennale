#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "montecarlo.h"
#include "pcg-c/include/pcg_variants.h"
#include "pcg-c/extras/entropy.h"

int main(int argc, char *argv[])
{
    // Initialize random number generator
    uint64_t seeds[2];
    entropy_getbytes((void*)seeds, sizeof(seeds));
    pcg32_srandom(seeds[0], seeds[1]);

    // TODO: fare in modo decente questa cosa
    if (!strncmp(argv[1], "path", 4))
    {
        char*    path        = argv[2];
        double   t           = atof(argv[3]);
        unsigned N           = atoi(argv[4]);
        unsigned eqSteps     = atoi(argv[5]);
        unsigned sampleSteps = atoi(argv[6]);
        unsigned Nmeas       = atoi(argv[7]);

        printf(
            "Out file path: %s\n"
            "t = %f\n"
            "N = %d\n"
            "eqSteps = %d\n"
            "sampleSteps = %d\n"
            "Nmeas = %d\n",
            path, t, N, eqSteps, sampleSteps, Nmeas
        );

        savePathData(path, t, N, eqSteps, sampleSteps, Nmeas);
    }
    else if (!strncmp(argv[1], "corr", 4))
    {
        char*    path1       = argv[2];
        char*    path2       = argv[3];
        double   t           = atof(argv[4]);
        unsigned N           = atoi(argv[5]);
        unsigned eqSteps     = atoi(argv[6]);
        unsigned sampleSteps = atoi(argv[7]);
        unsigned Nmeas       = atoi(argv[8]);
        unsigned tauMax      = atoi(argv[9]);
        unsigned tauStep     = atoi(argv[10]);

        printf(
            "Out file path 1: %s\n"
            "Out file path 2: %s\n"
            "t = %f\n"
            "N = %d\n"
            "eqSteps = %d\n"
            "sampleSteps = %d\n"
            "Nmeas = %d\n"
            "tauMax = %d\n"
            "tauStep = %d\n",
            path1, path2, t, N, eqSteps, sampleSteps, Nmeas, tauMax, tauStep
        );

        saveCorrData(path1, path2, t, N, eqSteps, sampleSteps, Nmeas, tauMax, tauStep);
    }
    else if (!strncmp(argv[1], "actionT", 7))
    {
        char*    path        = argv[2];
        double   t           = atof(argv[3]);
        unsigned Nmin        = atoi(argv[4]);
        unsigned Nmax        = atoi(argv[5]);
        unsigned Nstep       = atoi(argv[6]);
        unsigned eqSteps     = atoi(argv[7]);
        unsigned sampleSteps = atoi(argv[8]);
        unsigned Nmeas       = atoi(argv[9]);

        printf(
            "Action at fixed t\n"
            "Out file path: %s\n"
            "t = %f\n"
            "Nmin = %d\n"
            "Nmax = %d\n"
            "Nstep = %d\n"
            "eqSteps = %d\n"
            "sampleSteps = %d\n"
            "Nmeas = %d\n",
            path, t, Nmin, Nmax, Nstep, eqSteps, sampleSteps, Nmeas
        );

        saveActionDataFixedT(path, t, Nmin, Nmax, Nstep, eqSteps, sampleSteps, Nmeas);
    }
    else if (!strncmp(argv[1], "energyA", 7))
    {
        char*    path        = argv[2];
        double   a           = atof(argv[3]);
        unsigned Nmin        = atoi(argv[4]);
        unsigned Nmax        = atoi(argv[5]);
        unsigned Nstep       = atoi(argv[6]);
        unsigned eqSteps     = atoi(argv[7]);
        unsigned sampleSteps = atoi(argv[8]);
        unsigned Nmeas       = atoi(argv[9]);

        printf(
            "Energy at fixed a\n"
            "Out file path: %s\n"
            "a = %f\n"
            "Nmin = %d\n"
            "Nmax = %d\n"
            "Nstep = %d\n"
            "eqSteps = %d\n"
            "sampleSteps = %d\n"
            "Nmeas = %d\n",
            path, a, Nmin, Nmax, Nstep, eqSteps, sampleSteps, Nmeas
        );

        saveEnergyDataFixedA(path, a, Nmin, Nmax, Nstep, eqSteps, sampleSteps, Nmeas);
    }
    else if (!strncmp(argv[1], "energyT", 7))
    {
        char*    path        = argv[2];
        double   t           = atof(argv[3]);
        unsigned Nmin        = atoi(argv[4]);
        unsigned Nmax        = atoi(argv[5]);
        unsigned Nstep       = atoi(argv[6]);
        unsigned eqSteps     = atoi(argv[7]);
        unsigned sampleSteps = atoi(argv[8]);
        unsigned Nmeas       = atoi(argv[9]);

        printf(
            "Energy at fixed t\n"
            "Out file path: %s\n"
            "t = %f\n"
            "Nmin = %d\n"
            "Nmax = %d\n"
            "Nstep = %d\n"
            "eqSteps = %d\n"
            "sampleSteps = %d\n"
            "Nmeas = %d\n",
            path, t, Nmin, Nmax, Nstep, eqSteps, sampleSteps, Nmeas
        );

        saveEnergyDataFixedT(path, t, Nmin, Nmax, Nstep, eqSteps, sampleSteps, Nmeas);
    }
    else
        printf("Error: invalid command");

    return 0;
}
