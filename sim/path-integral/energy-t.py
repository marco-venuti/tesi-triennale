#!/usr/bin/env python3

import matplotlib.pyplot as plt
import numpy as np
from sys import argv
from scipy.optimize import curve_fit
from uncertainties import correlated_values
from utils import stdMean, blocking

plt.style.use('style.yml')

# estraggo i dati
data = np.loadtxt(argv[1], unpack=False)

if len(data) == data.size:
    data = np.array([data])

t = float(argv[2])
N = data[:, 0]
data = data[:, 1:]  # tolgo la prima colonna (quella con gli N)

# data = data[:, 10000:]

blockSizes = [1000] if True else range(1, int(len(data[0])/10), 1)
blockErr = []

# faccio blocking
for i, d in enumerate(data):
    print(f'\nN = {N[i]}')
    stds = []
    for blockSize in blockSizes:
        avg, std = blocking(d, blockSize)
        stds.append(std)
        print(f'{blockSize}: {avg:.5f} ± {std:.5f}')

    if len(blockSizes) == 1:
        blockErr.append(stds[0])
    else:
        plt.plot(blockSizes, stds, linestyle='-', marker='')
        plt.plot(
            [min(blockSizes), max(blockSizes)],
            2*[stdMean(d)]
        )
        plt.xlabel('Block size')
        plt.ylabel('Error')
        plt.title(f'Blocking: N = {N[i]}, t = {t}')

        plt.show()
        # plt.savefig(f'energy-a-block-N={N[i]}.png')
        plt.close()

if len(blockSizes) != 1:
    quit()

avgs = np.array([d.mean() for d in data])

for i, j in zip(avgs, blockErr):
    print(f'{i:.5f} ± {j:.5f}')


cusu = 0
avgs = avgs[cusu:]
N = N[cusu:]
blockErr = blockErr[cusu:]

# Faccio il fit
def fit(x, m, q):
    return m*x + q


opt, cov = curve_fit(
    fit, (t/N)**2, avgs, sigma=blockErr, absolute_sigma=True
)

chi2 = ((np.array(avgs) - fit((t/N)**2, *opt))**2 / np.array(blockErr)**2).sum()
dof = len(avgs)-len(opt)
print(f'chi2/dof = {chi2}/{dof}')

X = np.array([0, max(t/N)**2])
plt.plot(X, fit(X, *opt), color='blue')

opt = correlated_values(opt, cov)
E_0 = opt[1]
scarto = abs(0.5-E_0.nominal_value)/E_0.std_dev
print(f'E_0 = {E_0} ({scarto})')


plt.errorbar(
    (t/N)**2, avgs, yerr=blockErr,
    marker='.', markersize=3, linestyle='', color='red', capsize=1, capthick=0.5
)
plt.xlim(0)
plt.ylim(top=0.502)
plt.xlabel('$ a^2 $')
plt.ylabel('$ E_0 $')
plt.title(f'$ t = {t} $, block size = $ {blockSizes[0]} $')
plt.savefig('energy-t.png')
# plt.show()
