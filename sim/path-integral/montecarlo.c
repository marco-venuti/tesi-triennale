#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include "pcg-c/include/pcg_variants.h"

extern inline unsigned pmod(int i, int n)
{
    return (i % n + n) % n;
}

extern inline double myRandDouble()
{
    return ldexp(pcg32_random(), -32);
}

extern inline double uniformExtract(double x1, double x2)
{
    return x1 + myRandDouble()*(x2-x1);
}

extern inline double potential(double x)
{
    return 0.5*x*x; //  + (fabs(x) <= 0.1 ? 5 : 0);//+ lambda*x*x*x*x;
}

double* initializeTimeCrystal(unsigned N)
{
    double* crystal = (double*)malloc(N*sizeof(double));
    for (int i=0; i<N; i++)
        crystal[i] = 0;
    return crystal;
}

void fprintCrystal(double* crystal, unsigned N, FILE* file)
{
    for(unsigned i=0; i<N; i++)
        fprintf(file, "%f ", crystal[i]);
    fprintf(file, "\n");
}

double action(double* x, unsigned N, double a)
{
    double toReturn = 0;
    for(unsigned i=0; i<N; i++)
        toReturn +=
            0.5*(x[(i+1)%N]-x[i])*(x[(i+1)%N]-x[i])/a + a*potential(x[i]);
    /* 0.5*(x[(i+1)%N]-x[i])*(x[i]-x[pmod(i-1, N)])/a + a*potential(x[i]); */
    return toReturn;
}

void extractPath(double* x, unsigned steps, unsigned N, double a, double delta)
{
    double deltaS, xnew;
    unsigned start, site;

    for (unsigned step=0; step<steps; step++)
    {
        // sweep
        start = pcg32_boundedrand(N);  // start sweeping from a random site

        for (unsigned i=0; i<N; i++)
        {
            site = (start+i)%N;
            xnew = x[site] + delta*uniformExtract(-1, 1);

            // Metropolis move
            deltaS =
                a*(potential(xnew) - potential(x[site])) +
                (xnew - x[site])*(xnew + x[site] - x[(site+1)%N] - x[pmod(site-1, N)])/a;

            if (deltaS <= 0) // Surely move (downhill move)
                x[site] = xnew;
            else // Maybe move (uphill move)
                if (myRandDouble() < exp(-deltaS)) // ħ = 1
                    x[site] = xnew;
        }
    }
}

void savePathData(char* path, double t, unsigned N, unsigned eqSteps, unsigned sampleSteps, unsigned Nmeas)
{
    FILE* outFile = fopen(path, "w");
    double* crystal = initializeTimeCrystal(N);

    fprintf(
        outFile,
        "# t: %f, N: %d, eqSteps: %d, sampleSteps: %d\n", t, N, eqSteps, sampleSteps
    );

    // Reach equilibrium
    extractPath(crystal, eqSteps, N, t/N, 2.0*sqrt(t/N));

    // Measurements
    for(int i=0; i<Nmeas; i++)
    {
        extractPath(crystal, sampleSteps, N, t/N, 2.0*sqrt(t/N));
        fprintCrystal(crystal, N, outFile);
        printf("Done cycle %d/%d\n", i, Nmeas);
    }

    free(crystal);
    fclose(outFile);
}

void saveCorrData(
    char* path1, char* path2, double t, unsigned N, unsigned eqSteps, unsigned sampleSteps,
    unsigned Nmeas, unsigned tauMax, unsigned tauStep)
{
    FILE* outFile1 = fopen(path1, "w");
    FILE* outFile2 = fopen(path2, "w");

    fprintf(
        outFile1,
        "# t: %f, N: %d, eqSteps: %d, sampleSteps: %d\n", t, N, eqSteps, sampleSteps
    );
    fprintf(
        outFile2,
        "# t: %f, N: %d, eqSteps: %d, sampleSteps: %d\n", t, N, eqSteps, sampleSteps
    );

    // Initialize time-crystal
    double* crystal = (double*)malloc(N*sizeof(double));
    for (int i=0; i<N; i++)
        crystal[i] = 0;

    // Reach equilibrium
    extractPath(crystal, eqSteps, N, t/N, 2.0*sqrt(t/N));

    // Perform measurements
    double corr1, corr2;
    for(int i=0; i<Nmeas; i++)
    {
        // Calculate correlator at various distances
        for(int tau=0; tau<=tauMax; tau+=tauStep)
        {
            corr1 = 0;
            corr2 = 0;
            // Average correlator on the crystal
            for(int j=0; j<N; j++)
            {
                corr1 += crystal[(tau+j)%N]*crystal[j];
                corr2 += crystal[(tau+j)%N]*crystal[(tau+j)%N]*crystal[j]*crystal[j];
            }
            fprintf(outFile1, "%f ", corr1/N);
            fprintf(outFile2, "%f ", corr2/N);
        }
        fprintf(outFile1, "\n");
        fprintf(outFile2, "\n");
        printf("Done cycle %d/%d\n", i, Nmeas);
        extractPath(crystal, sampleSteps, N, t/N, 2.0*sqrt(t/N));
    }

    free(crystal);
    fclose(outFile1);
    fclose(outFile2);
}

void saveActionDataFixedT(char* path, double t, unsigned Nmin, unsigned Nmax, unsigned Nstep,
                          unsigned eqSteps, unsigned sampleSteps, unsigned Nmeas)
{
    FILE* outFile = fopen(path, "w");
    double* crystal;

    fprintf(
        outFile,
        "# t: %f, eqSteps: %d, sampleSteps: %d", t, eqSteps, sampleSteps
    );

    for(unsigned N=Nmin; N<=Nmax; N+=Nstep)
    {
        // Initialize time-crystal
        crystal = (double*)malloc(N*sizeof(double));
        for (int i=0; i<N; i++)
            crystal[i] = 0;

        // Reach equilibrium
        extractPath(crystal, eqSteps, N, t/N, 2.0*sqrt(t/N));

        // Perform measurements
        fprintf(outFile, "\n%d ", N);
        for(int i=0; i<Nmeas; i++)
        {
            extractPath(crystal, sampleSteps, N, t/N, 2.0*sqrt(t/N));
            fprintf(outFile, "%f ", action(crystal, N, t/N));
        }
        printf("Done %d ≤ %d ≤ %d\n", Nmin, N, Nmax);
        free(crystal);
    }

    fclose(outFile);
}

void saveEnergyDataFixedA(char* path, double a, unsigned Nmin, unsigned Nmax, unsigned Nstep,
                          unsigned eqSteps, unsigned sampleSteps, unsigned Nmeas)
{
    FILE* outFile = fopen(path, "w");
    double* crystal;
    double energy;

    fprintf(
        outFile,
        "# a: %f, eqSteps: %d, sampleSteps: %d", a, eqSteps, sampleSteps
    );

    for(unsigned N=Nmin; N<=Nmax; N+=Nstep)
    {
        // Initialize time-crystal
        crystal = (double*)malloc(N*sizeof(double));
        for (int i=0; i<N; i++)
            crystal[i] = 0;

        // Reach equilibrium
        extractPath(crystal, eqSteps, N, a, 2.0*sqrt(a));

        // Perform measurements
        fprintf(outFile, "\n%d ", N);
        for(int i=0; i<Nmeas; i++)
        {
            extractPath(crystal, sampleSteps, N, a, 2.0*sqrt(a));
            energy = 0;
            for(int j=0; j<N; j++)
                energy += potential(crystal[j]);
            fprintf(outFile, "%f ", 2*energy/N);
        }
        printf("Done %d ≤ %d ≤ %d\n", Nmin, N, Nmax);
        free(crystal);
    }

    fclose(outFile);
}

void saveEnergyDataFixedT(char* path, double t, unsigned Nmin, unsigned Nmax, unsigned Nstep,
                          unsigned eqSteps, unsigned sampleSteps, unsigned Nmeas)
{
    FILE* outFile = fopen(path, "w");
    double* crystal;
    double energy;
    double a;

    fprintf(
        outFile,
        "# t: %f, eqSteps: %d, sampleSteps: %d", t, eqSteps, sampleSteps
    );

    for(unsigned N=Nmin; N<=Nmax; N+=Nstep)
    {
        // Initialize time-crystal
        crystal = (double*)malloc(N*sizeof(double));
        for (int i=0; i<N; i++)
            crystal[i] = 0;

        a = t/N;

        // Reach equilibrium
        extractPath(crystal, eqSteps, N, a, 2.0*sqrt(a));

        // Perform measurements
        fprintf(outFile, "\n%d ", N);
        for(int i=0; i<Nmeas; i++)
        {
            extractPath(crystal, sampleSteps, N, a, 2.0*sqrt(a));
            energy = 0;
            for(int j=0; j<N; j++)
                energy += potential(crystal[j]);
            fprintf(outFile, "%f ", 2*energy/N);
        }
        printf("Done %d ≤ %d ≤ %d, a = %f\n", Nmin, N, Nmax, a);
        free(crystal);
    }

    fclose(outFile);
}
