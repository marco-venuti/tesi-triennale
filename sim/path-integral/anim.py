#!/usr/bin/env python3

import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation
import numpy as np
from sys import argv

data = np.loadtxt(argv[1], unpack=False)
N = len(data[0])  # crystal size
framerate = 30

plt.style.use('style.yml')
fig, ax = plt.subplots()
xdata, ydata = [], []
ln, = plt.plot([], [], marker='')


def init():
    delta = 15*data[:, 0].std(ddof=1)
    ax.set_xlim(-delta, delta)
    ax.set_ylim(0, N-1)
    ax.set_xlabel('$ x $')
    ax.set_ylabel('$ t/a $')
    return ln,


def update(frame):
    ln.set_data(data[frame], range(N))
    return ln,


ani = FuncAnimation(
    fig, update, frames=range(len(data)), interval=1000/framerate,
    init_func=init, blit=True, repeat_delay=1000
)

# plt.show()
ani.save('anim.mp4')
