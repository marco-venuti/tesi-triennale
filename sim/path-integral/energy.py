#!/usr/bin/env python3

import matplotlib.pyplot as plt
import numpy as np
from sys import argv
from utils import factors

plt.style.use('style.yml')

# Energia ad a fissato

# estraggo i dati
data = np.loadtxt(argv[1], unpack=False)

if len(data) == data.size:
    data = np.array([data])

a = float(argv[2])
N = data[:, 0]
data = data[:, 1:]  # tolgo la prima colonna (quella con gli N)
# data = data[:, 1:83161]

t = N*a


# calcolo l'errore senza tenere conto delle correlazioni
means = [
    d.mean()
    for d in data
]

stds = [
    d.std(ddof=1)/np.sqrt(len(d))
    for d in data
]

plt.errorbar(t, means, yerr=stds, linestyle='-', marker='.')
plt.xlabel('$ t $')
plt.ylabel('$ E_0 $')
plt.savefig('energy-a.png')
# plt.show()
plt.close()

# faccio blocking
for i, d in enumerate(data):
    print(f'\nN = {N[i]}, t = {t[i]}')

    blockSizes = []
    stds = []

    for blockSize in sorted(factors(len(d))):
        Nblocks = int(len(d)/blockSize)

        blocks = [
            d[i*blockSize: (i+1)*blockSize]
            for i in range(Nblocks)
        ]

        avgs = np.array([
            b.mean()
            for b in blocks
        ])

        std = avgs.std(ddof=1)/np.sqrt(Nblocks) if Nblocks > 1 else 0

        blockSizes.append(blockSize)
        stds.append(std)

        print(f'{blockSize}: {std:.5f}')

    print(avgs.mean())
    plt.plot(blockSizes, stds, linestyle='-', marker='x')
    plt.plot(
        [min(blockSizes), max(blockSizes)],
        2*[d.std(ddof=1)/np.sqrt(len(d))]
    )
    plt.xlabel('Block size')
    plt.ylabel('Error')
    plt.title(f'Blocking: N = {N[i]}, t = {t[i]}')

    # plt.show()
    plt.savefig(f'energy-a-block-N={N[i]}.png')
    plt.close()
