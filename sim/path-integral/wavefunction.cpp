#include "utils/utils.hpp"
#include <fstream>
#include <iostream>

using namespace std;

int main(int argc, char *argv[])
{
    string path = argv[1];
    double deltaX = atof(argv[2]);
    double a = atof(argv[3]);
    unsigned M = atoi(argv[4]);

    // Parameters
    auto X = linspace(-a, a, M);

    // Load data
    auto data = loadtxt(path);
    auto N = data[0].size();
    auto Nmean = data.size();

    // Do averages
    vector<double> Y;
    double sum = 0;
    for(auto x : X)
    {
        for(auto sample : data)
        {
            for(auto site : sample)
                if (deltaX > abs(site-x))
                    sum++;
        }
        sum /= N*Nmean*2*deltaX;
        Y.push_back(sum);
    }

    ofstream outFile("wavefunctionPlot.txt");
    for(int i=0; i<X.size(); i++)
        outFile << X[i] << " " << Y[i] << "\n";
    outFile.close();

    return 0;
}
