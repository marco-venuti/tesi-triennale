#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt
from sys import argv

data = np.loadtxt(argv[1])

plt.style.use('style.yml')
plt.plot(range(len(data)), data)
plt.xlabel('MC updates')
plt.ylabel(r'$\langle x^{(t)}x^{(0)} \rangle$')
# plt.show()
plt.savefig('corr-mc-time.png')
