#include "utils/utils.hpp"
#include <fstream>
#include <iostream>

using namespace std;

/**
 * Il comando da dare è della forma
 * ./path-integral path corr-path.txt <t> <N> <eqSteps> 1 <Nmeas>
 */
int main(int argc, char *argv[])
{
    string path = argv[1];
    unsigned kMax = atoi(argv[2]);

    // Load data
    auto data = loadtxt(path);
    auto N = data[0].size(); // crystal size
    auto Nmean = data.size();

    vector<double> corr;

    for(int i=0; i<kMax; i++)
    {
        vector<double> timeMean;
        for(int j=0; j<Nmean-i; j++)
        {
            vector<double> crystMean;
            for(int k=0; k<N; k++)
                crystMean.push_back(data[j][k]*data[j+i][k]);
            timeMean.push_back(mean(crystMean));
        }
        corr.push_back(mean(timeMean));
        cout << "Fatto " << i << " su " << kMax << "\n";
    }

    ofstream outFile("corr-mc-time-plot.txt");
    for(auto i : corr)
        outFile << i << "\n";
    outFile.close();

    return 0;
}
