#ifndef MONTECARLO_H
#define MONTECARLO_H

#include <stdio.h>

/**
 * Prints site positions on a single line
 * @param crystal The time-crystal vector
 * @param N Crystal size
 * @param file Output file
 */
void fprintCrystal(double* crystal, unsigned N, FILE* file);

/**
 * Computes the action of crystal \p x
 * @param x Array of crystal sites
 * @param N Number of Time-crystal sites
 * @param a Crystal spacing
 */
double action(double* x, unsigned N, double a);

/**
 * Performs \p steps Metropolis steps on crystal \p x
 * @param x Array of crystal sites
 * @param steps Metropolis steps to perform
 * @param N Number of Time-crystal sites
 * @param a Crystal spacing
 * @param delta Metropolis step maximum displacement
 */
void extractPath(double* x, unsigned steps, unsigned N, double a, double delta);

/**
 * Peforms Monte Carlo simulations and save \f$ N_{\mathrm{meas}} \f$ measurements (position of all sites in the crystal).
 * @param path Output file path
 * @param t Time-crystal volume
 * @param N Number of Time-crystal sites
 * @param eqSteps Number of Metropolis steps before first measurement is taken
 * @param sampleSteps Number of Metropolis steps between measurements
 * @param Nmeas Number of measurements to be taken
 */
void savePathData(char* path, double t, unsigned N, unsigned eqSteps, unsigned sampleSteps, unsigned Nmeas);

/**
 * Peforms Monte Carlo simulations and save \p Nmeas measurements of the correlators
 \f\[
     \langle x(\tau) x(0) \rangle \quad\text{and}\quad
     \langle x^{2}(\tau) x^{2}(0) \rangle
 \f\]
 already averaged on the crystal.
 * @param path1 Output file path for correlator of \f$ x \f$
 * @param path2 Output file path for correlator of \f$ x^2 \f$
 * @param t Time-crystal volume
 * @param N Number of Time-crystal sites
 * @param eqSteps Number of Metropolis steps before first measurement is taken
 * @param sampleSteps Number of Metropolis steps between measurements
 * @param Nmeas Number of measurements to be taken
 * @param tauMax Maximum value for \f$ \tau \f$ (the minimum being zero)
 * @param tauStep \f$ \tau \f$ increment
 */
void saveCorrData(
    char* path1, char* path2, double t, unsigned N, unsigned eqSteps,
    unsigned sampleSteps, unsigned Nmeas, unsigned tauMax, unsigned tauStep);

/**
 * Performs Monte Carlo simulations and save \p Nmeas measurements of the action
 * \f\[ S = \sum_{j=1}^{N} \left[ \frac{1}{2}\frac{(x_{j+1}-x_j)^{2}}{a} + V(x_{j})a \right] \f\]
 * at fixed \f$ t \f$ (crystal volume), while \f$ N \f$ is varied from \f$ N_{\mathrm{min}} \f$
 * to \f$ N_{\mathrm{max}} \f$ at steps of \f$ N_{\mathrm{step}} \f$.
 * @param path Output file path
 * @param t Time-crystal volume
 * @param Nmin Minimum number of Time-crystal sites
 * @param Nmax Maximum number of Time-crystal sites
 * @param Nstep \f$ N \f$ increment
 * @param eqSteps Number of Metropolis steps before first measurement is taken
 * @param sampleSteps Number of Metropolis steps between measurements
 * @param Nmeas Number of measurements to be taken
 */
void saveActionDataFixedT(char* path, double t, unsigned Nmin, unsigned Nmax, unsigned Nstep,
                          unsigned eqSteps, unsigned sampleSteps, unsigned Nmeas);

/**
 * Ground state energy for the harmonic oscillator, computed via the virial theorem
 * \f\[ E = 2\langle V \rangle \f\]
 * at fixed lattice spacing \f$ a \f$, while \f$ N \f$ is varied from \f$ N_{\mathrm{min}} \f$
 * to \f$ N_{\mathrm{max}} \f$ at steps of \f$ N_{\mathrm{step}} \f$.
 * @param path Output file path
 * @param a Time-crystal spacing
 * @param Nmin Minimum number of Time-crystal sites
 * @param Nmax Maximum number of Time-crystal sites
 * @param Nstep \f$ N \f$ increment
 * @param eqSteps Number of Metropolis steps before first measurement is taken
 * @param sampleSteps Number of Metropolis steps between measurements
 * @param Nmeas Number of measurements to be taken
 */
void saveEnergyDataFixedA(char* path, double a, unsigned Nmin, unsigned Nmax, unsigned Nstep,
                          unsigned eqSteps, unsigned sampleSteps, unsigned Nmeas);

/**
 * Ground state energy for the harmonic oscillator, computed via the virial theorem
 * at fixed time \f$ t \f$, while \f$ N \f$ is varied from \f$ N_{\mathrm{min}} \f$
 * to \f$ N_{\mathrm{max}} \f$ at steps of \f$ N_{\mathrm{step}} \f$.
 * @param path Output file path
 * @param t Time-crystal volume
 * @param Nmin Minimum number of Time-crystal sites
 * @param Nmax Maximum number of Time-crystal sites
 * @param Nstep \f$ N \f$ increment
 * @param eqSteps Number of Metropolis steps before first measurement is taken
 * @param sampleSteps Number of Metropolis steps between measurements
 * @param Nmeas Number of measurements to be taken
 */
void saveEnergyDataFixedT(char* path, double t, unsigned Nmin, unsigned Nmax, unsigned Nstep,
                          unsigned eqSteps, unsigned sampleSteps, unsigned Nmeas);

#endif // MONTECARLO_H
