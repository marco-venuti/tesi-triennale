import yaml
import numpy as np
from functools import reduce


def factors(n: int) -> set:
    """Restituisce i divisori di n"""
    return set(
        reduce(
            list.__add__,
            ([i, n//i] for i in range(1, int(n**0.5) + 1) if n % i == 0)
        )
    )


def get_parameters(filename: str) -> dict:
    """Get parameters from first line of a data file. Parameters are
    expected to be present in the first line in form
    # param1: value1, param2: value2, ..."""
    with open(filename, "r") as f:
        first_line = f.readline()

    if first_line[0] == '#':
        return yaml.safe_load(first_line.strip('# ').replace(', ', '\n'))

    return None


def stdMean(data: np.array) -> float:
    return data.std(ddof=1)/np.sqrt(len(data))


def blocking(data: np.array, blockSize: int) -> (float, float):
    Nblocks = int(len(data)/blockSize)

    blocks = [
        data[i*blockSize: (i+1)*blockSize]
        for i in range(Nblocks)
    ]

    avgs = np.array([
        b.mean()
        for b in blocks
    ])

    return (
        avgs.mean(),
        stdMean(avgs) if Nblocks > 1 else 0
    )
