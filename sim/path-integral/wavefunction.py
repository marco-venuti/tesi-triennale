#!/usr/bin/env python3

import matplotlib.pyplot as plt
import numpy as np
from sys import argv

X, Y = np.loadtxt(argv[1], unpack=True)

plt.style.use('style.yml')
plt.plot(X, np.exp(-X**2)/np.sqrt(np.pi), color='blue')
plt.plot(
    X, Y, marker='.', markersize=3, linestyle='', color='red',
)
plt.xlabel('$ x $')
plt.ylabel(r'$ \left|\psi_0^2(x)\right| $')
plt.legend([r'$ \pi^{-1/2} e^{-x^2} $', 'Data'])
# plt.show()
plt.savefig('wavefunction.png')
