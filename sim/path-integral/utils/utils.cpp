#include <vector>
#include <string>
#include <fstream>
#include <string.h>

using namespace std;

vector<double> split(string line)
{
    vector<double> toReturn;
    char* _line = const_cast<char*>(line.c_str());
    char* cusu = strtok(_line, " ");

    while(cusu != NULL)
    {
        toReturn.push_back(atof(cusu));
        cusu = strtok(NULL, " ");
    }
    return toReturn;
}

vector<double> linspace(double a, double b, unsigned N)
{
    vector<double> toReturn;
    double step = (b-a)/(N-1);
    for(int i=0; i<N; i++)
        toReturn.push_back(a+i*step);
    return toReturn;
}

vector<vector<double>> loadtxt(string path)
{
    ifstream file(path);
    vector<vector<double>> data;
    string line;
    while(getline(file, line))
    {
        if(line[0] == '#')
            continue;
        data.push_back(split(line));
    }
    return data;
}

double mean(vector<double> &vec)
{
    double sum = 0;
    for(int i=0; i<vec.size(); i++)
        sum += vec[i];
    return sum/vec.size();
}
