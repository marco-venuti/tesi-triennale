#ifndef UTILS_HPP
#define UTILS_HPP

#include <string>
#include <vector>

using namespace std;

// Math
vector<double> linspace(double a, double b, unsigned N);
double mean(vector<double> &vec);

// String operations
vector<double> split(string line);

// File operations
vector<vector<double>> loadtxt(string path);

#endif // UTILS_HPP
