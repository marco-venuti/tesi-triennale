#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

double f(double x)
{
    return exp(-x*x);
}

double distribution(double x, double a, double b)
{
    if (x < 0)
        x *= -1;
    if (x < a)
        return 2./5/a;
    else if (x >= a && x < b)
        return 1./10/b;
    else
        return 0.;
}

double fTilde(double x, double a, double b)
{
    return f(x)/distribution(x, a, b);
}

double uniformExtract(double x1, double x2)
{
    return x1 + (double)rand()/RAND_MAX*(x2-x1);
}

int main(int argc, char *argv[])
{
    // Integrale brutale
    double integral = 0;
    for (double i=-100; i<100; i+=0.1)
        integral += 0.1*f(i);

    printf("Brutale: %.50f\n", integral);

    // Importance sampling
    double a = 2;
    double b = 80;
    unsigned N = 1e8;

    srand(time(0));
    double sum = 0;
    for (int i=0; i<N; i++)
    {
        // Extract according to distribution
        double x;
        if ((double)rand()/RAND_MAX < 0.8)
            x = uniformExtract(0, a);
        else
            x = uniformExtract(a, b);
        if (rand() % 2)
            x *= -1;
        sum += fTilde(x, a, b);
    }
    printf("Importance sampling: %f\n", sum/N);

    return 0;
}
